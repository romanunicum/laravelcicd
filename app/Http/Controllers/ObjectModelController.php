<?php

namespace App\Http\Controllers;

use App\Models\ObjectModel;
use Illuminate\Http\Request;

class ObjectModelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ObjectModel $objectModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ObjectModel $objectModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ObjectModel $objectModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ObjectModel $objectModel)
    {
        //
    }
}
