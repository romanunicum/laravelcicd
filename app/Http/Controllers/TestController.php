<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ObjectModel;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        echo "get_current_user: " . get_current_user();
        echo "<br>";
        echo "system('whoami'): " . system('whoami');
        echo "<br>";
        // echo system('ls -la');
        echo "<br>";
        echo "Refreshed";
        echo "<br>";
        return 'Ok';
    }

    public function models(){
        return ObjectModel::all();
    }

    public function tables(){
        $tables = DB::select('SHOW TABLES');
        print_r($tables);//
    }
}
